import { Root } from "native-base";
import React from "react";
import AppNavigator from './navigation/AppNavigator';

export default class App extends React.Component {
  render() {
    return (<Root><AppNavigator /></Root>);
  }
}