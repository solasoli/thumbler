import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1, 
    alignItems: 'center', 
    paddingTop: 40,
    backgroundColor: "#0C5F55",
    paddingLeft:15,
    paddingRight:15
  },
  logo: {
    width: "100%", 
    height: 80, 
    resizeMode: "contain"
  },
  thumbler: {
    width: "100%", 
    height: 250, 
    marginTop: 30, 
    resizeMode: "contain"
  },
  countryCode: {
    justifyContent: 'center', 
    backgroundColor:"#fff",
    paddingLeft:15,
    paddingRight:10,
    borderTopLeftRadius: 25,
    borderBottomLeftRadius: 25,
  },
  activeTitle: {
    color: 'red',
  },
  phoneNumber: {
    backgroundColor: "#fff",
    paddingRight:25,
    alignSelf:"stretch",
    borderTopRightRadius: 25,
    borderBottomRightRadius: 25,
  }
});

