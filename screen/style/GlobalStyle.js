import { StyleSheet } from 'react-native';

export default StyleSheet.create({

  /* Button */
  button: {
    marginTop: 10,
    marginBottom:10
  },

  /* End Button */

  /* Global */
  textCenter: {
    textAlign: 'center'
  },
  textWhite: {
    color: "#fff"
  },
  textDanger: {
    color:"#ff0000"
  }
  /* End Global */
});

