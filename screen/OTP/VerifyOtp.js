import React from "react";
import { View, Text, TextInput, TouchableOpacity } from "react-native";
import otpStyle from '../style/OTPStyle';
import styles from '../style/GlobalStyle';
import OtpInputs from 'react-native-otp-inputs';
import { Button, Toast, Container, Content } from 'native-base';

class VerifyOtp extends React.Component {

  static navigationOptions = {
    //title: 'Welcome',
    header: null,
    isLoading: false,
  };

  constructor(props) {
    super(props);
    console.log(this.props)
    this.state = {
      phone: this.props.phoneNumber,
      errorMsg: ""
    }

    this.sendOtp = this.sendOtp.bind(this);
    this.verifyOtp = this.verifyOtp.bind(this);
  }

  sendOtp(phoneNumber){
    this.setState({
      isLoading: true
    });

    console.log(phoneNumber);
    fetch('http://192.168.100.31:8000/api/otp/send_otp', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({phoneNumber: phoneNumber})
    })
    .then((response) => {
      if(response.status == 200){
        return response.json();
        //return responseJson.movies;
      } else {
        console.log("Something went wrong, for response!", response);
        //return response.json();
        
        this.setState({
          isLoading: false,
          errorMsg: "Something went wrong!"
        });
      }

    })
    .then((responseJson) => {
      console.log(responseJson);
      this.setState({
        isLoading: false,
        errorMsg: responseJson.status == 'failed' ? responseJson.msg : ""
      });;
    })
    .catch((error) => {
      console.error(error);

      this.setState({
        isLoading: false,
        errorMsg: "Something went wrong!"
      });
    });
  }

  verifyOtp(otp){
    this.setState({
      isLoading: true
    });

    console.log(otp);
    fetch('http://192.168.100.2:8000/api/otp/verify_otp', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({otp_code: otp, phoneNumber: this.state.phone})
    })
    .then((response) => {
      if(response.status == 200){
        return response.json();
        //return responseJson.movies;
      } else {
        console.log("Something went wrong, for response!", response);
        // return response.json();

        this.setState({
          isLoading: false,
          errorMsg: "Something went wrong!"
        });
      }

    })
    .then((responseJson) => {
      if(responseJson.status == 'success'){
          //setting async storage

          this.props.navigation.push('HaveThumbler', {phoneNumber: phoneNumber});
      }

      this.setState({
        isLoading: false,
        errorMsg: responseJson.status == 'failed' ? responseJson.msg : ""
      });;
    })
    .catch((error) => {
      console.error(error);

      this.setState({
        isLoading: false,
          errorMsg: "Something went wrong!"
      });
    });
  }

  render() {
    const {navigate} = this.props.navigation;

    return (
      <Container style={[otpStyle.container]}>
        <Content contentContainerStyle={{ flexGrow: 1 }}>
          <View style={{flex: 1}}>
            <View style={{flexDirection: "row", marginBottom: 30,marginTop: 30}}>
              <View style={{flex:1}}>
              <OtpInputs
                handleChange={code => this.setState({otp: code})}
                containerStyles={styles.textWhite}
                inputStyles={styles.textWhite}
                inputsContainerStyles={styles.textWhite}
                inputContainerStyles={styles.textWhite}
                numberOfInputs={6}
              />
              </View>
            </View>

          </View>
        </Content>
      </Container>
    );
  }

}

export default VerifyOtp;