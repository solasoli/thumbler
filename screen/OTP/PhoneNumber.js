import React from "react";
import { View, Text, TextInput, TouchableOpacity,ActivityIndicator,ImageBackground, Image} from "react-native";
import otpStyle from '../style/OTPStyle';
import styles from '../style/GlobalStyle';
import { Button, Toast, Container, Content } from 'native-base';

class PhoneNumber extends React.Component {

  static navigationOptions = {
    //title: 'Welcome',
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      phone: "",
      isLoading: false,
      errorMsg: "",
      showToast: false
    }

    this.sendOtp = this.sendOtp.bind(this);
  }

  sendOtp(phoneNumber){
    this.setState({
      isLoading: true
    });

    console.log(phoneNumber);
    fetch('http://192.168.100.31:8000/api/otp/send_otp', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({phoneNumber: phoneNumber})
    })
    .then((response) => {
      if(response.status == 200){
        return response.json();
        //return responseJson.movies;
      } else {
        console.log("Something went wrong, for response!", response);
        //return response.json();
        
        this.setState({
          isLoading: false,
          errorMsg: "Something went wrong!"
        });
      }

    })
    .then((responseJson) => {
      console.log(responseJson);
      if(responseJson.status == 'success'){
          this.props.navigation.push('VerifyOtp', {phoneNumber: phoneNumber});
      }

      this.setState({
        isLoading: false,
        errorMsg: responseJson.status == 'failed' ? responseJson.msg : ""
      });;
    })
    .catch((error) => {
      console.error(error);

      this.setState({
        isLoading: false,
        errorMsg: "Something went wrong!"
      });
    });
  }

  render() {
    const {navigate} = this.props.navigation;
    return (
      <Container style={[otpStyle.container]}>
        <Content contentContainerStyle={{ flexGrow: 1 }}>
          <View>
            <ImageBackground source={require("../../assets/img/phone_number_abstract.png")} style={{width: "100%", height: "100%"}}>
              <View style={{flex: 1 , alignItems: 'center'}}>
                <Image source={require("../../assets/img/logo_white.png")} style={otpStyle.logo} />
              </View>
              <View style={{flex: 3 , alignItems: 'center'}}>
                <Image source={require("../../assets/img/thumbler_1.png")} style={otpStyle.thumbler} />
              </View>
              <View style={{flex: 1}}>
                <View style={{flexDirection: "row"}}>
                  <View style={otpStyle.countryCode}>
                      <Text>+62</Text>
                  </View>
                  <View style={{flex:1}}>
                    <TextInput
                      placeholder=""
                      keyboardType={'numeric'}
                      style={otpStyle.phoneNumber}
                      onChangeText={phone => this.setState({ phone: phone })}
                    />
                  </View>
                </View>

                {this.state.errorMsg != "" && Toast.show({
                  text: this.state.errorMsg,
                  buttonText: "Okay",
                  type: "danger",
                  duration: 3000
                })}

                {this.state.errorMsg != "" && this.setState({errorMsg: ""})}

                <Button style={styles.button} disabled={this.state.isLoading ? true : false} full rounded bordered light onPress={() => this.sendOtp(this.state.phone)}>
                  {!this.state.isLoading ? <Text style={[styles.textCenter, styles.textWhite]}>Send OTP</Text> :  <ActivityIndicator size="small" color="#ff0000" />}
                </Button>

                <Text style={[styles.textCenter, styles.textWhite]}>
                  By logging in or registering, you agree to our Terms of Service and Privacy Policy
                </Text>
              </View>
            
            </ImageBackground>
          </View>
        </Content>
      </Container>
    );
  }

}

export default PhoneNumber;